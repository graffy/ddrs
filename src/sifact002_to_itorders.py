#!/usr/bin/env python3
from pathlib import Path
import re
import pandas
import argparse
import tempfile
import logging

# example of sifac_t002 file
# ```
# 	Centre financier	Elément d'OTP	Document d'achat	Fournisseur/Division fourn.	Date du document	Code TVA	Désignation	Date livraison	Valeur totale en cours	Reste à livrer (quantité)	Livraison finale	Reste à facturer (quantité)	Facture finale	Immobilisation	Groupe marchandises	Texte Correspondance	Créateur commande
# 0	991R423	17CQ423-A0	4500454464.0	4692       AIR LIQUIDE FRANCE INDUSTRIE	2019-01-21	DJ	REGUL 2018GAZ HORS MARCHE ALPHAGAZ 2 AIR	2019-01-21	130.27	0		0			GA.21	Rejet à la demande de Magali	MARCAULT MAGALI
# ...
# ```


# converts a cnrs sifac type t002 report to a single table
def sifact002_to_sheet(sifac_t002_path: str, out_tsv_file_path: Path):

    dept_names = [
        'DIRECTION',
        'MAT.NANO.SCIENCES',
        'MATERIAUX ET LUMIERE',
        'MATIERE MOLLE',
        'MECA VERRES',
        'MILIEU DIVISE',
        'PHYSIQUE MOLECULAIRE'
    ]

    with open(out_tsv_file_path, 'wt', encoding='utf8') as outf:
        out_header_has_been_written = False
        for dept_name in dept_names:
            src_file_path = Path(sifac_t002_path.replace('<dept-id>', dept_name))
            tmp_file_path = Path(tempfile.NamedTemporaryFile(delete=False).name).with_suffix('.tsv')
            logging.debug('tmp_file_path for department %s : %s', dept_name, tmp_file_path)
            df = pandas.read_excel(src_file_path)
            df.to_csv(tmp_file_path, sep='\t')

            with open(tmp_file_path, encoding='utf8') as inf:
                in_header_has_been_read = False
                for line in inf.readlines():
                    if in_header_has_been_read:
                        outf.write(line)
                    else:
                        # this line is the header only output it if it's the 1st header
                        in_header_has_been_read = True
                        if not out_header_has_been_written:
                            outf.write(line)
                            out_header_has_been_written = True

    # add an index
    df = pandas.read_csv(out_tsv_file_path, sep='\t')
    # df.rename(columns={0: 'uid'})
    # df['id'] = range(1, len(df) + 1)
    # df.insert(0, 'id', df.pop('id'))
    # df = df[-1:] + df[:-1]  # put the id column in the first column
    df.to_csv(out_tsv_file_path, sep='\t')


def sifact002_to_itorders(sifac_t002_path: str, itorders_file_path: Path):
    all_orders_file_path = tempfile.NamedTemporaryFile(delete=False).name
    logging.debug('all_orders_file_path = %s', all_orders_file_path)

    sifact002_to_sheet(sifac_t002_path, all_orders_file_path)

    df = pandas.read_csv(all_orders_file_path, sep='\t')

    # delete the colums for which the label is of the form 'Unnamed: <n>'. They come from the csv export of libre office
    unnamed_columns = [column_label for column_label in df.keys() if re.match(r'^Unnamed', column_label) is not None]
    print(unnamed_columns)
    df = df.drop(columns=unnamed_columns)

    print(df.columns)
    print(df.keys())
    print(df)
    it_df = df[(df['Fournisseur/Division fourn.'] == '7976       DELL') | 
               (df['Fournisseur/Division fourn.'] == '16783      ECONOCOM') | 
               (df['Fournisseur/Division fourn.'] == '4945       MISCO INMAC WSTORE')
               ]

    # 'AMAZON EU SARL SUCCURSALE FRANCAISE'
    # 'INMAC'
    # 'RETIS'
    # 'APIXIT'
    print(it_df)

    # print(it_df[['Montant facturé', 'Raison sociale fournisseur', 'Libellé commande']])
    it_df.to_csv(itorders_file_path, sep='\t')


def main():
    logging.basicConfig(level=logging.DEBUG)

    arg_parser = argparse.ArgumentParser(description='extracts the orders related to information technology hardware purchases from excel sheets (one per department) containing all the orders')
    arg_parser.add_argument('--sifac-t002-path', type=str, required=True, help='the path to the input files, where the department is represented by the tag <dept-id> (eg ./achats-ipr/2019/ur1/from_ddemorel/2019.<dept-id>.xls)')
    arg_parser.add_argument('--it-orders-sheet', type=Path, required=True, help='the output sheet containing only it orders')
    args = arg_parser.parse_args()
    sifac_t002_path = args.sifac_t002_path
    it_orders_sheet_path = args.it_orders_sheet
    sifact002_to_itorders(sifac_t002_path, it_orders_sheet_path)


main()
