#!/usr/bin/env python3
from pathlib import Path
import re
import pandas
import argparse
import tempfile
import os

# a geslab_t002 file is a sheet that contains data in the following form:

# ```
# 					LABORATOIRE :		LABO1     IPR  - UNIVERSITE DE RENNES													Le		:		27/01/2023					
# 					DETAIL DES LIGNES DE COMMANDE															Exercice		:		2019					
# 																				Page			:		1		/		100
# Etablissement				:		DR17			Délégation Bretagne et Pays de la Loire																				
# 																													
# 																													
# N° com. GESLAB	Date commande	N° ligne	Libellé ligne					Raison sociale fournisseur		S	Qté com.	Reste à const.	Qté fac.	Cons. ligne antérieur	Consommé ligne	Réservé ligne	Facturé ligne	Code origine	Elément analytique		Code EDP					Matière		Nat. dép.	
# ...
# 18,313.00	04/01/19	43,427.00	POUSSE SERINGUE PHD ULTRA 4400 I/W					HARVARD APPARATUS		S	1	0	1	0.00	6,616.00	0.00	6,616.00	59190	591901		ANRASJ					2500		IM	
# ```


# converts a cnrs geslab type t002 report to a single table
def geslabt002_to_sheet(in_tsv_file_path: Path, out_tsv_file_path: Path):

    with open(in_tsv_file_path, encoding='utf8') as inf, open(out_tsv_file_path, 'wt', encoding='utf8') as outf:
        table_header_has_been_written = False
        for line in inf.readlines():

            is_table_header = re.match(r'^N° com. GESLAB', line) is not None
            # for some strange reason, the column 'N° com. GESLAB''s contents are alternatively something like '1952-12-17 12:00:00 AM' and something like '19,855.00'
            if is_table_header and not table_header_has_been_written:
                outf.write('# %s' % line)
                table_header_has_been_written = True
            if re.match(r'^[0-9,.]+\t', line):
                outf.write(line)
            elif re.match(r'^[0-9][0-9][0-9][0-9]-[0-9]+-[0-9]+ [0-9][0-9]:[0-9][0-9]:[0-9][0-9] [AP]M\t', line):
                outf.write(line)
            else:
                print('ignoring line : %s' % line)


def geslabt002_to_itorders(geslabt002_file_path: Path, itorders_file_path: Path):

    all_orders_file_path = tempfile.NamedTemporaryFile(delete=False).name
    print(f'all_orders_file_path = {all_orders_file_path}')
    geslabt002_to_sheet(geslabt002_file_path, all_orders_file_path)

    df = pandas.read_csv(all_orders_file_path, sep='\t')

    # delete the colums for which the label is of the form 'Unnamed: <n>'. They come from the csv export of libre office
    unnamed_columns = [column_label for column_label in df.keys() if re.match(r'^Unnamed', column_label) is not None]
    print(unnamed_columns)
    df = df.drop(columns=unnamed_columns)

    print(df.columns)
    print(df.keys())
    print(df)
    PETIT_MATERIEL_INFORMATIQUE = '1100'
    EQUIPEMENT_INFORMATIQUE = '2100'
    INFORMATIQUE_ACHAT = 'D3--'
    it_df = df[(df['Matière'] == PETIT_MATERIEL_INFORMATIQUE) | (df['Matière'] == EQUIPEMENT_INFORMATIQUE) | (df['Matière'] == INFORMATIQUE_ACHAT)]
    print(it_df)

    # to remove clutter, drop the columns that we don't need
    print(it_df.keys())
    it_df = it_df.drop(columns=['# N° com. GESLAB'])  # this column seems to contain anything but ordering number
    it_df = it_df.drop(columns=['N° ligne'])  # I don't know the meaning of this column
    it_df = it_df.drop(columns=['Code origine'])  # I don't know the meaning of this column
    it_df = it_df.drop(columns=['Elément analytique'])  # I don't know the meaning of this column
    it_df = it_df.drop(columns=['S'])  # I don't know the meaning of this column

    print(it_df[['Facturé ligne', 'Raison sociale fournisseur', 'Libellé ligne']])
    os.makedirs(itorders_file_path.parent, exist_ok=True)
    it_df.to_csv(itorders_file_path, sep='\t')


def main():
    arg_parser = argparse.ArgumentParser(description='extracts the orders related to information technology hardware purchases from a report (in format geslab_t002) containing all the orders')
    arg_parser.add_argument('--all-orders-report', type=Path, required=True, help='the input report file (in format geslab_t002) containing all the orders')
    arg_parser.add_argument('--it-orders-sheet', type=Path, required=True, help='the output sheet containing only it orders')
    args = arg_parser.parse_args()
    all_orders_report_path = args.all_orders_report
    it_orders_sheet_path = args.it_orders_sheet
    geslabt002_to_itorders(all_orders_report_path, it_orders_sheet_path)


main()
