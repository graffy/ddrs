#!/usr/bin/env python3
from pathlib import Path
import re
import pandas


# converts a cnrs geslab type t001 report to a single table
def geslabt001_to_sheet(in_tsv_file_path: Path, out_tsv_file_path: Path):
    with open(in_tsv_file_path, encoding='utf8') as inf, open(out_tsv_file_path, 'wt', encoding='utf8') as outf:
        table_header_has_been_written = False
        for line in inf.readlines():
            # the input report file contains groups of data lines sparated by header blocks such as this:
            # Entité dépensière : 				AESJULLIEN		AES RENNES METROPOLE MC JULLIEN									Crédits reçus : 								40,000.00
            # 															Disponible : 								24,743.14
            #
            #
            # N° commande	Souche		Libellé commande				Date commande	Raison sociale fournisseur	Montant consommé sur exercice antérieur	Montant consommé sur l'exercice			Montant réservé					Montant facturé		Code origine	Nature dépense	Statut		Cde groupée
            is_column_description = re.match(r'^N° commande', line) is not None
            if is_column_description and not table_header_has_been_written:
                outf.write('# %s' % line)
                table_header_has_been_written = True
            if re.match(r'^[0-9,./]+\t', line):
                # this line is expected to contain data (ie not be part of the header blocks)
                # 19,572.00	19AESMCJ		CAMERA ZYLA 5.5 sCMOS				04/11/19	ANDOR TECHNOLOGY LIMITED	0.00	13,681.56			0.00					0.00		635991	IM
                outf.write(line)


def geslabt001_to_itorders(geslabt001_file_path: Path, itorders_file_path: Path):
    sheet_file_path = Path('./tmp/commandes-2019-cnrs.tsv')
    geslabt001_to_sheet(geslabt001_file_path, sheet_file_path)

    df = pandas.read_csv(sheet_file_path, sep='\t')

    # delete the colums for which the label is of the form 'Unnamed: <n>'. They come from the csv export of libre office
    unnamed_columns = [column_label for column_label in df.keys() if re.match(r'^Unnamed', column_label) is not None]
    print(unnamed_columns)
    df = df.drop(columns=unnamed_columns)

    print(df.columns)
    print(df.keys())
    print(df)
    it_df = df[(df['Raison sociale fournisseur'] == 'DELL SAS') | (df['Raison sociale fournisseur'] == 'ECONOCOM PRODUCTS & SOLUTIONS')]
    # 'AMAZON EU SARL SUCCURSALE FRANCAISE'
    # 'INMAC'
    # 'RETIS'
    # 'APIXIT'
    print(it_df)

    print(it_df[['Montant facturé', 'Raison sociale fournisseur', 'Libellé commande']])
    it_df.to_csv(itorders_file_path, sep='\t')


def main():
    geslabt001_to_itorders(Path('./achats-ipr/2019/cnrs/from_ngicquiaux_20230118/commandes-2019-cnrs-t001.tsv'), Path('./tmp/commandes-it-2019-cnrs-001.tsv'))


main()
