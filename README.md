Projet créé pour remplir la partie matériel informatique de [labo 1.5](https://apps.labos1point5.org/ges-1point5/962)

# description des fichiers

- achats-ipr
	- 2019
		- cnrs
			- from_ngiquiaux
				- [commandes-2019-cnrs-t001.xls](achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t001.xls) : Ce fichier contient l'ensemble des achats de 2019, avec le libellé et le fournisseur. ngiquiaux a obtenu ce fichier par un des types d'extraction (todo: trouver son nom) de `gestlab`. Ca ressemble davantage à un rapport qu'une base de données, vu que l'unique feuille de calcul contient en fait une série de tableaux du même type, à raison d'un tableau par `Entité dépensière`. Un seul tableau avec une colonne entité `Entité dépensière` aurait été plus facile à exploiter.
					- fichier envoyé par ngiquiaux à graffy le `Wed, 18 Jan 2023 14:55:29 +0100`. 
				- [commandes-2019-cnrs-t001.tsv](achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t001.tsv) : [commandes-2019-cnrs-t001.xls](achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t001.xls) après conversion au format `tsv` avec `libreoffice`
				- [commandes-2019-cnrs-t002.xls](achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t002.xls) : Suite à une demande de graffy signalant à ngiquiaux qu'il manquait le code nacres dans [commandes-2019-cnrs-t001.xls](achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t001.xls), ngiquiaux a obtenu ce fichier par un des types d'extraction (todo: trouver son nom, mais dedans je me souviens qu'il y avait le mot *détail*) de `gestlab`, après en avoir essayés pluieurs autres.
					- fichier envoyé par ngiquiaux à graffy le `Thu, 26 Jan 2023 17:39:10 +0100`
				- [cnrs-matieres-001.csv](achats-ipr/2019/cnrs/from_ngicquiaux/cnrs-matieres-001.csv) : ce fichier est censé décrire les codes des matières utilisés dans [commandes-2019-cnrs-t002.xls](achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t002.xls)
					- fichier envoyé par ngiquiaux à graffy le `Thu, 26 Jan 2023 17:39:10 +0100`
			- [commandes-it-2019-cnrs-002.tsv](achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t002.tsv) : copie de [tmp/commandes-it-2019-cnrs-002.tsv](tmp/commandes-it-2019-cnrs.tsv) manuellement annotée par graffy le 03/02/2023 pendant la création de  [it-cnrs-l1p5.tsv](achats-ipr/2019/cnrs/it-l1p5.tsv)
			- [it-cnrs-l1p5.tsv](achats-ipr/2019/cnrs/it-l1p5.tsv) : achats it cnrs 2019 au format attendu par `labo1.5`
				- manuellement créé par graffy le 03/02/2023 à partir de [commandes-it-2019-cnrs-002.tsv](achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t002.tsv)
		- ur1
			- from_ddemorel
				- `2019.<dept-id>.xls` : 
					- fichiers envoyés par ddemorel à graffy le `07/02/2023`
			- [commandes-it-2019-ur1-002-annotated.tsv](achats-ipr/2019/ur1/commandes-it-2019-ur1-002-annotated.tsv)
- tmp
	- [commandes-it-2019-cnrs-002.tsv](tmp/commandes-it-2019-cnrs-t002.tsv) : itorder for cnrs in 2019
		- built from [commandes-2019-cnrs-t002.tsv](achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t002.tsv) using [geslabt002_to_itorders.py](src/geslabt002_to_itorders.py)


# journal

## 2023/01/26

- graffy a demandé à ddemorel de faire une extraction achats ur1 2019 comme celle qui avait été faite pour 2021
- graffy vu qu'il est impossible d'isoler de façon automatique la totalité des achats informatiques (on peut certes identifier les fournisseurs 'informatique' tels que dell ou econocom, mais certains matériels informatiques sont achetés chez des généralistes tels que amazon ou radiospares) à partir de `from-cloud.ipr/2019/commandes-2019-cnrs-t001.xls`, car il manque quelque chose du style code nacres. graffy a donc demandé à ngiquiaux de voir si elle ne pouvait pas obtenir une extraction avec toutes les infos concernant tous les achats. Elle m'a envoyé `from-cloud.ipr/2019/commandes-2019-cnrs-t002.xls` qui ne contient finalement pas du tout les détails des achats (fausse manip ?)

## 2023/01/27

- graffy a obtenu de ngicquiaux un nouveau fichier `from-cloud.ipr/2019/commandes-2019-cnrs-t002.xls` qui semble correct cette fois!

## 2023/02/07

- graffy a reçu de ddemorel les fichiers suivants

```log
graffy@graffy-ws2:~/work/ddrs$ ls -l ./achats-ipr/2019/ur1/from_ddemorel_20230207/
total 2288
-rw-r--r-- 1 graffy spm 369152 mars   3 16:51  2019.DIRECTION.xls
-rw-r--r-- 1 graffy spm 318976 mars   3 16:51 '2019.MATERIAUX ET LUMIERE.xls'
-rw-r--r-- 1 graffy spm 269312 mars   3 16:51 '2019.MATIERE MOLLE.xls'
-rw-r--r-- 1 graffy spm 326656 mars   3 16:51  2019.MAT.NANO.SCIENCES.xls
-rw-r--r-- 1 graffy spm 341504 mars   3 16:51 '2019.MECA VERRES.xls'
-rw-r--r-- 1 graffy spm 224256 mars   3 16:51 '2019.MILIEU DIVISE.xls'
-rw-r--r-- 1 graffy spm 479744 mars   3 16:51 '2019.PHYSIQUE MOLECULAIRE.xls'
```

## 2023/03/03

### généré it-cnrs-l1p5.tsv

critique:
- colonnes utiles
	- quantité
- libelles tronqués à 35 caracteres qui empêchent de voir le modèle exact (exemple CAT2_Configuration n°6 Latitude 559 au lieu de CAT2_Configuration n°6 Latitude 5590)
- duplication
	- id659 == id828 ? (3 my passport)
	- id623 == id1030 ? (station d'accueil dmorineau)
	- id1919 == id2007 ? (5 disques durs, id1919 n'a pas de prix..?)
- libellé vague id926 : "petits matériels informatiques"
- pourquoi souris, et pas:
	- dock (station d'accueil)
	- adaptateur vga
	- cartouche d'encre ?
	- alimentation magsafe
	- barre de son externe
- pourquoi imprimante id1170 et pas scanner id1171 ?
- id1268	disque dur à 521.98 € ?
- carte raid perc id1776
- il manque les prix parfois, et ça n'aide pas à savoir ce que c'est : id1919
- id2303: ACER D1301 impossible de savoir ce que c'est
- id3204 gopro ?
- barrettes de ram id2468
- quantité pas fiable ? id2600 plusieurs barrettes mémoire (sans doute car prix élevé 874€) alors que quantité = 1
- id2767 samsung 860 evo le libellé ne précise pas la capacité (cette même réf est utilisée pour des disques dur de différent capacité)


## 2023/03/06

### généré it-ur1-l1p5.tsv

critique:
	- peu de lignes comparé à cnrs... est-ce normal (22 ur1 vs 84 cnrs)?
	- libellé incomplet
		- id527	- l'écran n'apparait même pas dans le libellé alors que je sais qu'il y en a un
		- beaucoup de libellés qui ne contiennent pas le numéro de modèle

### uploadé it-l1p5.tsv dans labo1.5
	- seul le scanner a été rejeté (pas de catégorie)
	- station d'accueil manque dans la doc labo 1.5:
		En fait, après importation, j'ai pu constater qu'il il y a 15 types de matériel dans labo1.5:
		- pc fixe sans écran
		- pc fixe tout-en-un
		- pc portable
		- écran
		- videoprojecteur
		- tablette
		- smartphone
		- imprimante
		- téléphone ip
		- station d'accueil
		- clavier souris
		- borne wifi
		- serveur
		- disque dur
		- gpu puissant


		alors que la doc ne mentionne que ces 14 types:
		- PC fixe (Desktop, PC, fixe, tour, unité centrale, station de travail, workstation)
		- PC portable (laptop, portable, notebook)
		- Serveur
		- Écran (monitor, display, screen)
		- Vidéo projecteur
		- Tablette (pad)
		- Smartphone (mobile)
		- Imprimante
		- Borne wifi (Wifi hub)
		- Téléphone IP
		- GPU
		- Disque dur
		- Clavier
		- Souris


## 2024/10/10

### rédaction d'une procédure pour le matériel info du labo 1.5

Il s'agit d'abord de retrouver ce qui a été fait

```sh
20241010-22:29:45 graffy@graffy-ws2:~/work/ddrs$ ls -l ./achats-ipr/2019/cnrs/
total 48
-rw-r--r-- 1 graffy spm 25212 mars   6  2023 commandes-it-2019-cnrs-002-annotated.tsv
-rw-r--r-- 1 graffy spm  6153 mars   6  2023 commandes-it-2019-ur1-002-annotated.tsv
drwxr-xr-x 2 graffy spm  4096 mars   6  2023 from_ngicquiaux
-rw-r--r-- 1 graffy spm  4140 mars   6  2023 it-l1p5.tsv
last command status : [0]
```

J'ai créé https://git.ipr.univ-rennes.fr/graffy/ddrs.git pour faciliter la consultation de l'historique

```sh
git remote add origin https://git.ipr.univ-rennes.fr/graffy/ddrs.git
20241010-22:41:21 graffy@graffy-ws2:~/work/ddrs$ git push -u origin main
error: src refspec main does not match any
error: failed to push some refs to 'https://git.ipr.univ-rennes.fr/graffy/ddrs.git'
last command status : [1]
20241010-22:41:43 graffy@graffy-ws2:~/work/ddrs$ git branch
* master
last command status : [0]
20241010-22:42:12 graffy@graffy-ws2:~/work/ddrs$ git branch -m master main
last command status : [0]
20241010-22:42:53 graffy@graffy-ws2:~/work/ddrs$ git push -u origin main
Enumerating objects: 54, done.
Counting objects: 100% (54/54), done.
Delta compression using up to 8 threads
Compressing objects: 100% (42/42), done.
Writing objects: 100% (54/54), 171.79 KiB | 3.82 MiB/s, done.
Total 54 (delta 18), reused 0 (delta 0)
remote: . Processing 1 references
remote: Processed 1 references in total
To https://git.ipr.univ-rennes.fr/graffy/ddrs.git
 * [new branch]      main -> main
Branch 'main' set up to track remote branch 'main' from 'origin'.
last command status : [0]
```

# 11/10/2024

