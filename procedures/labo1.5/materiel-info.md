# Comment créer la base de données partie matériel informatique pour labo 1.5

## description du fichier attendu par labo1.5

- un tableau type excel
- formats de fichier acceptés par labo 1.5:
	- tsv (tabular separated value)
- description des colonnes:
	1. **id**: par exemple `ur1_237`
	2. **Modele**: par exemple `latitude 3400`
	3. **Fabricant**: par exemple `dell`
	4. **Type**: par exemple `pc portable`

### types supportés par labo 1.5

La documentation de labo1.5 mentionne ces 14 types:
- PC fixe (Desktop, PC, fixe, tour, unité centrale, station de travail, workstation)
- PC portable (laptop, portable, notebook)
- Serveur
- Écran (monitor, display, screen)
- Vidéo projecteur
- Tablette (pad)
- Smartphone (mobile)
- Imprimante
- Borne wifi (Wifi hub)
- Téléphone IP
- GPU
- Disque dur
- Clavier
- Souris

En fait, après importation, j'ai pu constater qu'il il y a 15 types de matériel dans labo1.5 (station d'accueil a du être oublié dans la doc):
- pc fixe sans écran
- pc fixe tout-en-un
- pc portable
- écran
- videoprojecteur
- tablette
- smartphone
- imprimante
- téléphone ip
- station d'accueil
- clavier souris
- borne wifi
- serveur
- disque dur
- gpu puissant

## vue d'ensemble de la procédure

![procédure](materiel-info-graph.svg)

note: commande utilisée pour créer [materiel-info-graph.svg](materiel-info-graph.svg) à partir de [materiel-info-graph.dot](materiel-info-graph.dot):

```sh
20241011-15:49:16 graffy@graffy-ws2:~/work/ddrs/procedures/labo1.5$ dot ./materiel-info-graph.dot -Tsvg > ./materiel-info-graph.svg
last command status : [0]
```

## description des fichiers

### cnrs

#### cnrs_all_xls

Ce fichier contient l'extraction geslab de tous les achats (pas seulement informatique) de l'année considérée.

- exemple : [commandes-2019-cnrs-t002.xls](../../achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t002.xls)
- colonnes:
	1. **N° com. GESLAB**: par exemple `18360`
	2. **Date commande**: par exemple `24/01/19`
	3. **N° ligne**: identifiant unique de l'achat, par exemple `43543`
	4. **Libellé ligne**: par exemple `CAT2_Configuration n°3 XPS 9365`
	9. **Raison sociale fournisseur**: par exemple `DELL SAS`
	11. **S**: par exemple `S`
	12. **Qté com.**: par exemple `1`
	13. **Reste à const.**: par exemple `0`
	14. **Qté fac.**: par exemple `1`
	15. **Cons. ligne antérieur**: par exemple `0.00`
	16. **Consommé ligne**: par exemple `1,755.00`
	17. **Réservé ligne**: par exemple `0.00`
	18. **Facturé ligne**: par exemple `1,755.00`
	19. **Code origine**: par exemple `SE9ADO0985`
	20. **Elément analytique**: par exemple `9ADO0985`
	22. **Code EDP**: par exemple `NANOSC`
	27. **Matière**: le code matière, dont la liste se trouve dans [cnrs-matieres-001.csv](../../achats-ipr/2019/cnrs/from_ngicquiaux/cnrs-matieres-001.csv) par exemple `2100`
	29. **Nat. dép.**: par exemple `IM`

#### cnrs_all_tsv

- exemple : [commandes-2019-cnrs-t002.tsv](../../achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t002.tsv), qui contient 1991 lignes de données
- colonnes: les mêmes que [cnrs_all_xls](####-cnrs_all_xls)

[commandes-2019-cnrs-t002.tsv](../../achats-ipr/2019/cnrs/from_ngicquiaux/commandes-2019-cnrs-t002.tsv)
```tsv
					LABORATOIRE :		LABO1     IPR  - UNIVERSITE DE RENNES													Le		:		27/01/2023					
					DETAIL DES LIGNES DE COMMANDE															Exercice		:		2019					
																				Page			:		1		/		100
Etablissement				:		DR17			Délégation Bretagne et Pays de la Loire																				
																													
																													
N° com. GESLAB	Date commande	N° ligne	Libellé ligne					Raison sociale fournisseur		S	Qté com.	Reste à const.	Qté fac.	Cons. ligne antérieur	Consommé ligne	Réservé ligne	Facturé ligne	Code origine	Elément analytique		Code EDP					Matière		Nat. dép.	
18,310.00	03/01/19	43,424.00	CULOTTE SIMPLE PVC FEMELLE					AMAZON EU SARL SUCCURSALE FRANCAISE		S	1		1	0.00	12.63	0.00	12.63	584081	584081		ANRSON					1900		FO	
1950-2-17 12:00:00 AM	03/01/19	2018-11-21 12:00:00 AM	REGISTRATION P. PANIZZA AERC 2019					SLOVENIAN SOCIETY FOR EXPERIMENTAL MECHANICS - SSEM SLOVENSKO DRUSTVO ZA EKSPERIMENTALNO MEHANIKO		S	1	0	1	0.00	530.00	0.00	530.00	9CTD0985	9CTD0985		CIFAF2					5400		FO	
18,313.00	04/01/19	43,427.00	POUSSE SERINGUE PHD ULTRA 4400 I/W					HARVARD APPARATUS		S	1	0	1	0.00	6,616.00	0.00	6,616.00	59190	591901		ANRASJ					2500		IM	
...
1950-4-7 12:00:00 AM	24/01/19	2019-3-19 12:00:00 AM	CAT2_Configuration n°3 XPS 9365					DELL SAS		S	1	0	1	0.00	1,755.00	0.00	1,755.00	SE9ADO0985	9ADO0985		NANOSC					2100		IM	
...
```

Attention: le 11/10/2024, graffy s'est rendu compte que certaines données de ce fichier ont été mal converties par l'exporteur tsv. L'achat 43543 aurait du apparaître comme ceci:

```
18360	24/01/19	43543	CAT2_Configuration n°3 XPS 9365					DELL SAS		S	1	0	1	0.00	1,755.00	0.00	1,755.00	SE9ADO0985	9ADO0985		NANOSC					2100		IM
```

Au lieu de ça, il apparaît ainsi (les colonnes **N° com. GESLAB** et **N° ligne** ont été converties en date, pour une raison inconnue):
```
1950-4-7 12:00:00 AM	24/01/19	2019-3-19 12:00:00 AM	CAT2_Configuration n°3 XPS 9365					DELL SAS		S	1	0	1	0.00	1,755.00	0.00	1,755.00	SE9ADO0985	9ADO0985		NANOSC					2100		IM	
```

#### cnrs_it_orders

Contient uniquement les achats informatiques extraits de [cnrs_all_tsv](####-cnrs_all_tsv).

- exemple: [commandes-it-2019-cnrs-002.tsv](../../tmp/commandes-it-2019-cnrs-002.tsv), qui contient 224 lignes de données
- colonnes: les mêmes que pour [cnrs_all_tsv](####-cnrs_all_tsv)

exemple:
```tsv
	Date commande	Libellé ligne	Raison sociale fournisseur	Qté com.	Reste à const.	Qté fac.	Cons. ligne antérieur	Consommé ligne	Réservé ligne	Facturé ligne	Code EDP	Matière	Nat. dép.
71	24/01/19	MPXT2FN/A	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	1,195.15	0.00	1,195.15	MECVER	2100	IM
72	24/01/19	MBP13-3PL	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	89.00	0.00	89.00	MECVER	2100	IM
73	24/01/19	ECO-0,30	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	0.30	0.00	0.30	MECVER	2100	IM
74	24/01/19	CTO-MPXT2-16GB	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	170.00	0.00	170.00	MECVER	2100	IM
...
84	24/01/19	D31591	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	40.78	0.00	40.78	MECVER	2100	IM
89	24/01/19	CAT2_Configuration n°3 XPS 9365	DELL SAS	1.0	0.0	1.0	0.0	1,755.00	0.00	1,755.00	NANOSC	2100	IM
90	24/01/19	Dell Wireless Mouse WM126 BLACK	DELL SAS	1.0	0.0	1.0	0.0	8.00	0.00	8.00	NANOSC	2100	IM
91	24/01/19	Eco contribution	DELL SAS	1.0	0.0	1.0	0.0	0.43	0.00	0.43	NANOSC	2100	IM
...
```

#### cnrs_it_orders_labo1p5

- au format [attendu par labo 1.5](##description-du-fichier-attendu-par-labo1.5)
- par exemple [it-cnrs-l1p5.tsv](../../achats-ipr/2019/it-cnrs-l1p5.tsv) qui contien 84 lignes de données:

```tsv
id	Modele	Fabricant	Type
cnrs_71	macbook pro 13 (2017)	apple	pc portable
cnrs_79	brilliance 258B6QUEB	philips	écran
cnrs_89	xps 9365	dell	pc portable
cnrs_90	wm126	dell	souris
cnrs_210	latitude 5590	dell	pc portable
...
```

### ur1

#### ur1_all_orders

- exemple: [from_ddemorel](../../achats-ipr/2019/ur1/from_ddemorel) (610 items) qui contient:
	- [2019.DIRECTION.xls](<../../achats-ipr/2019/ur1/from_ddemorel/2019.DIRECTION.xls>), (93 items).
	- [2019.MAT.NANO.SCIENCES.xls](<../../achats-ipr/2019/ur1/from_ddemorel/2019.MAT.NANO.SCIENCES.xls>), (85 items).
	- [2019.MATERIAUX ET LUMIERE.xls](<../../achats-ipr/2019/ur1/from_ddemorel/2019.MATERIAUX ET LUMIERE.xls>), (86 items).
	- [2019.MATIERE MOLLE.xls](<../../achats-ipr/2019/ur1/from_ddemorel/2019.MATIERE MOLLE.xls>), (62 items).
	- [2019.MECA VERRES.xls](<../../achats-ipr/2019/ur1/from_ddemorel/2019.MECA VERRES.xls>), (88 items).
	- [2019.MILIEU DIVISE.xls](<../../achats-ipr/2019/ur1/from_ddemorel/2019.MILIEU DIVISE.xls>), (45 items).
	- [2019.PHYSIQUE MOLECULAIRE.xls](<../../achats-ipr/2019/ur1/from_ddemorel/2019.PHYSIQUE MOLECULAIRE.xls>), (151 items).
- colonnes:
	1. **Centre financier**: par exemple `991R423`
	2. **Elément d'OTP**: par exemple `17CQ423-S0`
	3. **Document d'achat**: par exemple `4500458349.0`
	4. **Fournisseur/Division fourn.**: par exemple `7976       DELL`
	5. **Date du document**: par exemple `2019-02-14`
	6. **Code TVA**: par exemple `DK`
	7. **Désignation**: par exemple `LATITUDE 5590 - Portable conf.n°2`
	8. **Date livraison**: par exemple `2019-02-25`
	9. **Valeur totale en cours**: par exemple `1616.8`
	10. **Reste à livrer (quantité)**: par exemple `0`
	11. **Livraison finale**: par exemple `X`
	12. **Reste à facturer (quantité)**: par exemple `0`
	13. **Facture finale**: par exemple `126639.0`
	14. **Immobilisation**: par exemple ``
	15. **Groupe marchandises**: par exemple `IA.11`
	16. **Texte Correspondance**: par exemple `Danièle, Le numéro d'ordre n'est pas cor`
	17. **Créateur commande**: par exemple, `DE MOREL DANIELE`


#### ur1_it_orders

Contient uniquement les achats informatiques extraits de [ur1_all_orders](####-ur1_all_orders).

- exemple: [commandes-it-2019-ur1-002\.tsv](../../tmp/commandes-it-2019-ur1-002.tsv), qui contient 33 lignes de données
- colonnes: les mêmes que pour [ur1_all_orders](####-ur1_all_orders) mais avec la première colonne insérée
	1. **<anonyme>**: identifieur de ligne, par exemple `21`
	2. **Centre financier**: par exemple `991R423`
	3. **Elément d'OTP**: par exemple `17CQ423-S0`
	4. **Document d'achat**: par exemple `4500458349.0`
	5. **Fournisseur/Division fourn.**: par exemple `7976       DELL`
	6. **Date du document**: par exemple `2019-02-14`
	7. **Code TVA**: par exemple `DK`
	8. **Désignation**: par exemple `LATITUDE 5590 - Portable conf.n°2`
	9. **Date livraison**: par exemple `2019-02-25`
	10. **Valeur totale en cours**: par exemple `1616.8`
	11. **Reste à livrer (quantité)**: par exemple `0`
	12. **Livraison finale**: par exemple `X`
	13. **Reste à facturer (quantité)**: par exemple `0`
	14. **Facture finale**: par exemple `126639.0`
	15. **Immobilisation**: par exemple ``
	16. **Groupe marchandises**: par exemple `IA.11`
	17. **Texte Correspondance**: par exemple `Danièle, Le numéro d'ordre n'est pas cor`
	18. **Créateur commande**: par exemple, `DE MOREL DANIELE`

exemple:
```tsv
20241011-10:57:24 graffy@graffy-ws2:~/work/ddrs/procedures/labo1.5$ head -3 /home/graffy/work/ddrs/tmp/commandes-it-2019-ur1-002.tsv
	Centre financier	Elément d'OTP	Document d'achat	Fournisseur/Division fourn.	Date du document	Code TVA	Désignation	Date livraison	Valeur totale en cours	Reste à livrer (quantité)	Livraison finale	Reste à facturer (quantité)	Facture finale	Immobilisation	Groupe marchandises	Texte Correspondance	Créateur commande
21	991R423	17CQ423-S0	4500458349.0	7976       DELL	2019-02-14	DK	LATITUDE 5590 - Portable conf.n°2	2019-02-25	1616.8	0	X	0		126639.0	IA.11	Danièle, Le numéro d'ordre n'est pas cor	DE MOREL DANIELE
38	991R423	17CQ423-S0	4500463111.0	7976       DELL	2019-03-28	DJ	Acessoires pour LATITUDE 5 DELL	2019-04-15	81.74	0	X	0	X		IA.24	BC soldé mail Danièle 14/06/19 Danièle,	DE MOREL DANIELE
last command status : [0]
```

#### ur1_it_orders_ann

Version manuellement annotée de [ur1_it_orders].
- colonnes: les mêmes 18 colonnes que pour [ur1_it_orders](####ur1_it_orders), plus:
	19. **comment**: colonne utilisée pour les annotations, par exemple, `latitude 7490 + écran u2719d`

- exemple : [commandes-it-2019-ur1-002-annotated.tsv](../../achats-ipr/2019/ur1/commandes-it-2019-ur1-002-annotated.tsv), qui contient 33 lignes de données

Voici les differences entre [commandes-it-2019-ur1-002.tsv](../../tmp/commandes-it-2019-ur1-002.tsv) et [commandes-it-2019-ur1-002-annotated.tsv](../../achats-ipr/2019/ur1/commandes-it-2019-ur1-002-annotated.tsv):

```diff
20241011-10:07:17 graffy@graffy-ws2:~/work/ddrs/procedures/labo1.5$ diff /home/graffy/work/ddrs/tmp/commandes-it-2019-ur1-002.tsv /home/graffy/work/ddrs/achats-ipr/2019/ur1/commandes-it-2019-ur1-002-annotated.tsv
1c1
<       Centre financier        Elément d'OTP   Document d'achat        Fournisseur/Division fourn.     Date du document        Code TVA        Désignation     Date livraison  Valeur totale en cours      Reste à livrer (quantité)       Livraison finale        Reste à facturer (quantité)     Facture finale  Immobilisation  Groupe marchandises     Texte Correspondance        Créateur commande
---
>       Centre financier        Elément d'OTP   Document d'achat        Fournisseur/Division fourn.     Date du document        Code TVA        Désignation     Date livraison  Valeur totale en cours      Reste à livrer (quantité)       Livraison finale        Reste à facturer (quantité)     Facture finale  Immobilisation  Groupe marchandises     Texte Correspondance        Créateur commande       comment
6c6
< 233   991R423 17CQ423-G0      4500479590.0    7976       DELL 2019-10-01      DK      PC portable DELL pour MARIETTE Céline   2019-10-28      1474.61 0       X       0          127281.0 IA.11   num IMMO = 127281 num etiquette  052108 LECLAIRE FRANCIS
---
> 233   991R423 17CQ423-G0      4500479590.0    7976       DELL 2019-10-01      DK      PC portable DELL pour MARIETTE Céline   2019-10-28      1474.61 0       X       0          127281.0 IA.11   num IMMO = 127281 num etiquette  052108 LECLAIRE FRANCIS        modèle ?
20,22c20,22
< 509   991R423 17CQ423-A0      4500466536.0    7976       DELL 2019-05-02      DK      ORDINATEUR PORTABLE LUCILE RUTKOWSKI    2019-05-15      2205.84 0       X       0          126799.0 IA.11   immo: 126799 code barre: 051358 MARCAULT MAGALI
< 527   991R423 17CQ423-A0      4500469692.0    7976       DELL 2019-05-28      DK      Ordinateur portable Guillaume Raffy     2019-06-17      1981.57 0       X       0       X  126871.0 IA.11   numéro immo 126871 étiquette 051357     MARCAULT MAGALI
< 530   991R423 17CQ423-A0      4500472335.0    7976       DELL 2019-06-25      DK      PORTABLE LATITUDE DEV. 100000519892     2019-07-01      1680.16 0       X       0       X  126927.0 IA.11   immo: 126927 Etiquette: 051641  DE MOREL DANIELE
---
> 509   991R423 17CQ423-A0      4500466536.0    7976       DELL 2019-05-02      DK      ORDINATEUR PORTABLE LUCILE RUTKOWSKI    2019-05-15      2205.84 0       X       0          126799.0 IA.11   immo: 126799 code barre: 051358 MARCAULT MAGALI modèle ? + écran ?
> 527   991R423 17CQ423-A0      4500469692.0    7976       DELL 2019-05-28      DK      Ordinateur portable Guillaume Raffy     2019-06-17      1981.57 0       X       0       X  126871.0 IA.11   numéro immo 126871 étiquette 051357     MARCAULT MAGALI latitude 7490 + écran u2719d
> 530   991R423 17CQ423-A0      4500472335.0    7976       DELL 2019-06-25      DK      PORTABLE LATITUDE DEV. 100000519892     2019-07-01      1680.16 0       X       0       X  126927.0 IA.11   immo: 126927 Etiquette: 051641  DE MOREL DANIELE        modèle ?
last command status : [1]
```

[commandes-it-2019-ur1-002-annotated.tsv](../../achats-ipr/2019/ur1/commandes-it-2019-ur1-002-annotated.tsv)
```tsv
...
233	991R423	17CQ423-G0	4500479590.0	7976       DELL	2019-10-01	DK	PC portable DELL pour MARIETTE Céline	2019-10-28	1474.61	0	X	0		127281.0	IA.11	num IMMO = 127281 num etiquette  052108	LECLAIRE FRANCIS	modèle ?
237	991R423	17CQ423-G0	4500481443.0	7976       DELL	2019-10-14	DJ	CAT1_Configuration n°1 - Latitude 3400	2019-10-28	900.04	0	X	0			IA.11		LECLAIRE FRANCIS
...
```

#### ur1_it_orders_labo1p5

- au format [attendu par labo 1.5](##description-du-fichier-attendu-par-labo1.5)
- par exemple[it-l1p5.tsv](../../achats-ipr/2019/it-ur1-l1p5.tsv):
```tsv
id	Modele	Fabricant	Type
...
ur1_233		dell	pc portable
ur1_237	latitude 3400	dell	pc portable
...
```

## description des opérations

### `op_extra_geslab`: extraction geslab

Extraction geslab (effectuée par Nathalie Guicquiaux pour les données 2019)

### `op_extra_sifac`: extraction sifac

Extraction sifac (effectuée par Danièle Demorel pour les données 2019)

### `op_cnrs_it_filter`: filtrage pour ne garder que les achats informatiques

Opération effectuée automatiquement par [geslabt002_to_itorders.py](../../src/geslabt002_to_itorders.py) via [la recette make](../../src/Makefile)

Un achat est considéré comme un achat informatique que si le champ `Matière` a l'une des valeurs suivantes (définies dans [cnrs-matieres-001.csv](../../achats-ipr/2019/cnrs/from_ngicquiaux/cnrs-matieres-001.csv)):
- `1100` (PETIT_MATERIEL_INFORMATIQUE)
- `2100` (EQUIPEMENT_INFORMATIQUE)
- `D3--` (INFORMATIQUE_ACHAT)

### `op_anno_cnrs_it`: annotations

C'est l'opération manuelle qui consiste à ajouter une colonne `comment` pour y placer des infos supplémentaires concernant les achats, car la colonne `Libellé ligne` est souvent cryptique (il faut alors se référer aux archives papier des commandes cnrs `cnrs_paper_orders`), comme dans l'exemple ci-dessous où:
- le libellé `99MO084201` ne permet pas de savoir acilement qu'il s'agit d'un adaptatateur vga
- le libellé `MBP13-3PL` ne permet pas de savoir facilement qu'il s'agit de la garantie 3 ans du macbook pro
- etc.

```tsv
	Date commande	Libellé ligne	Raison sociale fournisseur	Qté com.	Reste à const.	Qté fac.	Cons. ligne antérieur	Consommé ligne	Réservé ligne	Facturé ligne	Code EDP	Matière	Nat. dép.
...
71	24/01/19	MPXT2FN/A	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	1,195.15	0.00	1,195.15	MECVER	2100	IM	macbook pro
72	24/01/19	MBP13-3PL	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	89.00	0.00	89.00	MECVER	2100	IM	garantie 3 ans
73	24/01/19	ECO-0,30	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	0.30	0.00	0.30	MECVER	2100	IM
74	24/01/19	CTO-MPXT2-16GB	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	170.00	0.00	170.00	MECVER	2100	IM	passage à 16Go RAM
75	24/01/19	CTO-MPXT2-SSD1To	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	531.25	0.00	531.25	MECVER	2100	IM	passage à disque dur 1To 
76	24/01/19	MI4-MBP13-5PL	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	106.00	0.00	106.00	MECVER	2100	IM	passage à garantie 5 ans
77	24/01/19	99MO084201	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	23.34	0.00	23.34	MECVER	2100	IM	adapter usbc->vga moshi 99MO084201
78	24/01/19	ECO-0,02	ECONOCOM PRODUCTS & SOLUTIONS	1.0	0.0	1.0	0.0	0.02	0.00	0.02	MECVER	2100	IM
...
```

### `op_cnrs_to_l1p5`: nettoyage et mise au format labo 1.5

C'est l'opération manuelle qui consiste à:
1. ajouter les colonnes labo 1.5: **id**, **Modèle**, **Fabricant** et **Type**
2. supprimer les lignes qui ne rentrent dans aucun [types supportés par labo 1.5](###-types-supportés-par-labo-1.5), par exemple, adaptateur vga ou extension de garantie
3. remplir les colonnes labo 1.5 (**id** est rempli par l'item number, préfixé par `cnrs_`, de manière à retrouver l'achat correspondant dans les tableaux détaillés, par exemple `ur1_it_orders_ann`)
4. supprimer les colonnes autres que les 4 colonnes labo 1.5

### `op_ur1_it_filter`: filtrage pour ne garder que les achats informatiques

Opération effectuée automatiquement par [sifact002_to_itorders.py](../../src/sifact002_to_itorders.py) via [la recette make](../../src/Makefile)

Un achat est considéré comme un achat informatique que si le champ `Fournisseur/Division fourn.` a l'une des valeurs suivantes:
- `7976       DELL`: marché pour l'achat de fixe et de portables non Apple
- `16783      ECONOCOM`: marché pour achat de matériel Apple
- `4945       MISCO INMAC WSTORE`: marché pour fournitures informatiques

### `op_ur1_to_l1p5`: nettoyage et mise au format labo 1.5

idem `op_cnrs_to_l1p5` mais côté ur1

### `op_concat`: fusion des données cnrs et ur1

Opération effectuée automatiquement par [la recette make](../../src/Makefile)